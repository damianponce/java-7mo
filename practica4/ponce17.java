import java.util.Scanner;

public class ponce17 { public static void main(String[] args) {
  Scanner in = new Scanner(System.in);
  System.out.print("Multiplos de ");
  int num = Integer.valueOf( in.nextLine() );
  int mult[] = new int[10];
  int sum = 0;

  for (int i=0; i<10; i++) {
    mult[i] = num*(i+1);
    System.out.print("/" + mult[i]); }
  System.out.println("/\n");

  for (int i : mult) if ( i%2 == 0 ) sum += i;
  System.out.println("La suma (con if) de los pares es " + sum);
  sum = 0;

  for (int i : mult) {
    while (i%2 == 0) {
      sum += i;
      break; } }
  System.out.println("La suma (sin if) de los pares es " + sum);
}}
