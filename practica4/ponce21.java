import java.util.Scanner;

public class ponce21 { public static void main(String[] args) {
  Scanner in = new Scanner(System.in);
  System.out.print("Categoría: ");
  char cat = in.nextLine().charAt(0);
  System.out.print("Antiguedad: ");
  int age = Integer.valueOf( in.nextLine() );
  System.out.print("Sueldo: ");
  int salary = Integer.valueOf( in.nextLine() );
  double raise = 0; double net = 0;

  if ( age >= 1 && age <= 5 ) { raise = salary * 1.05; }
  else if ( age >= 6 && age <= 10 ) { raise = salary * 1.1; }
  else if ( age > 10 ) { raise = salary * 1.3; }

  if ( cat == 'a' || cat == 'A' ) { net = raise + 1000; }
  else if ( cat == 'b' || cat == 'B' ) { net = raise + 2000; }
  else if ( cat == 'c' || cat == 'C' ) { net = raise + 3000; }

  System.out.println("\nEl aumento por antiguedad es de $" + (raise-salary));
  System.out.println("El aumento por categoria es de $" + (net-raise));
  System.out.println("El sueldo neto es de $" + net);
}}
